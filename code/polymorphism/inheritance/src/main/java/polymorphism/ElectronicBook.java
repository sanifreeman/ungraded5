package polymorphism.inheritance;

public class ElectronicBook extends Book{
    private int numberBytes;

    public ElectronicBook(String title, String author, int numberBytes){
        super(title, author);
        this.numberBytes=numberBytes;
    }
    public int getNumberBytes(){
return this.numberBytes;
    }
    public String toString(){
        String answer=super.toString();
        return answer+"this is the numberof bytes"+this.answer;
    }
}